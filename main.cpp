#include <array>
#include <cstdio>
#include <cstring>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdlib>
#include <fstream>
#include "src/Template.h"

using namespace std;

#ifdef __unix__
  #define UNIX 1
#elif defined (_WIN32) || defined(_WIN32)
  #define UNIX 0
#endif

// Constantes
char const *add[] = {"add", "-a"};
char const *init[] = {"init", "-i"};
char const *clean[] = {"clean", "-c"};
char const *help[] = {"help", "-h"};

// Funciones

void imprimeAyuda();
void iniciar();
void limpiar();
void noReconocido(char const *argv[], int argc);
int agregarClase(char const clase[]);
int indice(char const *cadenas[], char const * argv[], int argc);
bool archivoExiste (char const nombre[]);
bool folderExiste(char const *path);

int main(int argc, char const *argv[]){
  int index;
  if (argc == 2){
    // HELP
    index = indice(help, argv, argc);
    if (index>0) {
      imprimeAyuda();
      return 0;
    }
    // INIT
    index = indice(init, argv, argc);
    if (index>0){
      printf("Iniciando Proyecto...\n");
      iniciar();
      return 0;
    }
    // CLEAN
    index = indice(clean, argv, argc);
    if (index>0){
      printf("Limpiando...\n");
      limpiar();
      return 0;
    }else{
      noReconocido(argv, argc);
      imprimeAyuda();
      return 0;
    }
  }else if (argc == 3) {
    // ADD
    index = indice(add, argv, argc);
    if (index>0) {
      char const * clase = argv[index + 1];
      int i = agregarClase(clase);
      if (i) printf("Clase: %s agregada...", clase);
      return 0;
    }else{
      noReconocido(argv, argc);
      imprimeAyuda();
    }
  }
  else {
    imprimeAyuda();
  }
  return 0;
}

void imprimeAyuda(){
  printf("_  _ ____ ____ ____\n");
  printf("|\\/| |__| |__/ [__  \n");
  printf("|  | |  | |  \\ ___]\n");
  printf("Herramienta de gestión para proyectos C/C++, con Makefile\n");
  printf("Menú de Ayuda\n");
  printf("  -i, init:  Iniciar un nuevo proyecto.\n");
  printf("  -c, clean: Elimina archivos de configuración y compilación (Makefile).\n");
  printf("  -a, add:   Añade un nuevo archivo de clase en el directorio src/.\n");
  printf("             Uso: mars add <Clase>\n");
  printf("  -h, help:  Muestra este menú.\n");
}

void iniciar(){
  // BUILD
  if(!folderExiste("build")){
    system("mkdir build");
    printf("::Directorio build creado.\n");
  }else printf ("-Directorio build ya existe\n");

  // SRC
  if (!folderExiste("src")){
    system("mkdir src");
    printf("::Directorio src creado.\n");
  }
  else printf("-Directorio src ya existe\n");

  // OBJECT
  if (!folderExiste("object")){
    system("mkdir object");
    printf("::Directorio object creado.\n");
  }
  else printf("-Directorio build ya existe\n");
  
  // TESTS
  if (!folderExiste("tests")){
    system("mkdir tests");
    printf("::Directorio tests creado.\n");
  }
  else printf("-Directorio build ya existe\n");

  // TOUCH Makefile
  if (!archivoExiste("Makefile")) {
    FILE *file = fopen("Makefile", "w");
    fputs(Template::makefile(), file);
    fclose(file);
    printf("::Archivo Makefile creado.\n");
  }else printf("-Archivo Makefile ya existe.\n");

  // TOUCH MAIN
  if (!archivoExiste("main.cpp")){
    FILE *file = fopen("main.cpp", "w");
    fputs(Template::main(), file);
    fclose(file);
    printf("::Archivo main.cpp creado.\n");
  }
  else printf("-Archivo main.cpp ya existe.\n");

  // TOUCH TEST_MAKER
  if (!archivoExiste("test_maker.ipynb")){
    FILE *file = fopen("test_maker.ipynb", "w");
    fputs(Template::testMaker(), file);
    fclose(file);
    printf("::Archivo test_maker.ipynb creado.\n");
  }
  else printf("-Archivo test_maker.ipynb ya existe.\n");
  
 
  // TOUCH TEST
  if (!archivoExiste("test.bash")){
    FILE *file = fopen("test.bash", "w");
    fputs(Template::test(), file);
    fclose(file);
    printf("::Archivo test.bash creado.\n");
  }
  else printf("-Archivo test.bash ya existe.\n");
}

int agregarClase(char const clase[]){
  int length = strlen(clase);
  if (length>30) {
    printf("Identificador de clase es demasiado largo (> 30)\n");
    return 0;
  }
  char headerRuta[36];
  char defRuta[38];
  sprintf(headerRuta, "src/%s.h", clase);
  sprintf(defRuta, "src/%s.cpp", clase);

  if (!archivoExiste(headerRuta)){
    ofstream headerFile;
    headerFile.open(headerRuta);
    headerFile << "#ifndef " << clase <<"_h"<< std::endl;
    headerFile << "#define " << clase <<"_h"<< std::endl;
    headerFile<<""<<std::endl;
    headerFile<<"class "<<clase<<"{"<<std::endl;
    headerFile<<"private:"<<std::endl;
    headerFile<<"public:"<<std::endl;
	  headerFile<<"  "<<clase<<"(){}"<<std::endl;
    headerFile<<"};"<<std::endl;
    headerFile<<""<<std::endl;
    headerFile<<"#endif"<<std::endl;

    headerFile.close();
  } else printf("Archivo %s.h ya existe.\n", clase);

  if (!archivoExiste(defRuta)){
    ofstream claseFile;
    claseFile.open(defRuta);
    claseFile << "#include \""<<clase<<".h\""<< std::endl;
    claseFile.close();
  } else printf("Archivo %s.cpp ya existe.\n", clase);

  if (!archivoExiste("Makefile")) {iniciar();}

  ifstream inFile;
  inFile.open("Makefile");
  ofstream outFile;
  outFile.open("~Makefile");

  std::string content;
  int linea = 0;
  while (std::getline(inFile, content)) {
    if (linea==2){
      outFile<<"object/"<<clase<<".o\\"<<std::endl;
    }
    outFile<<content<<std::endl;
    linea++;
  }
  outFile<<std::endl;
  outFile<<"object/"<<clase<<".o: src/"<<clase<<".h src/"<<clase<<".cpp"<<std::endl;
	outFile<<"\t$(CC) $(CFLAGS) -c src/"<<clase<<".cpp -o object/"<<clase<<".o"<<std::endl;
  inFile.close();
  outFile.close();
  system((UNIX ? "rm Makefile" : "del Makefile"));
  system((UNIX ? "mv ~Makefile Makefile" : "rename ~Makefile Makefile"));
  return 1;
}

void limpiar(){
  // RM object
  if (folderExiste("object")) {
    printf("::Eliminando directorio object/\n");
    system((UNIX ? "rm -rf object" : "rmdir /s /q object"));
  }else printf("No se encontró directorio object.\n");

  //RM build
  if (folderExiste("build")){
    printf("::Eliminando directorio build/\n");
    system((UNIX ? "rm -rf build" : "rmdir /s /q build"));
  }
  else printf("No se encontró directorio build.\n");

  //RM make.bat
  if (archivoExiste("make.bat")) {
    printf("::Eliminando archivo make.bat\n");
    system((UNIX ? "rm make.bat" : "del make.bat"));
  } else printf("No se encontró archivo make.bat.\n");

  if (archivoExiste("Makefile")){
    printf("::Eliminando archivo Makefile\n");
    system((UNIX ? "rm Makefile" : "del Makefile"));
  }
  else
    printf("No se encontró archivo Makefile.\n");
}

void noReconocido(char const *argv[], int argc){
  printf("Comando no reconocido:\n");
  printf("  ");
  for (int i = 0; i < argc; i++){
    printf("%s ", argv[i]);
  }
  printf("\n");
}

int indice(char const *cadenas[], char const * argv[], int argc){
  for(int i = 0; i < 2; i++){
    for(int j = 0; j<argc; j++){
      if (std::strcmp(cadenas[i], argv[j]) == 0) {
        return j;
      }
    }
  }
  return -1;
}

bool archivoExiste(char const nombre[]){
  if (FILE *file = fopen(nombre, "r")){
    fclose(file);
    return true;
  }else{
    return false;
  }
}

bool folderExiste(char const *path){
  struct stat info;
  if (stat(path, &info) != 0)
    return 0;
  else if (info.st_mode & S_IFDIR)
    return 1;
  else
    return 0;
}