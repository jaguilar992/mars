# Mars
*Gestión de Proyectos C/C++*

***Desarrollo:***
*José Antonio Aguilar (jaguilar992)*

_ _ _
[TOC]
_ _ _

## 1. Uso
**Menú de Ayuda**

* **-i, init**:  Iniciar un nuevo proyecto.
* **-c, clean**: Elimina archivos de configuración y compilación (Makefile).
* **-a, add**:   Añade un nuevo archivo de clase en el directorio src/.
* **-h, help**:  Muestra este menú.

## 2. Ejemplo
```
λ mars init
Iniciando Proyecto...
::Directorio build creado.
::Directorio src creado.
::Directorio object creado.
::Archivo Makefile creado.
::Archivo main.cpp creado.
::Archivo make.bat creado.

λ mars add Clase
Clase: Clase agregada...

# src/
#   |__Clase.cpp
#   |__Clase.h

λ mars clean
Limpiando...
::Eliminando directorio object/
::Eliminando directorio build/
::Eliminando archivo make.bat
::Eliminando archivo Makefile
```
## Compilando con make
Comandos:
1. Compilar:
```
λ make
```
2. Compilar y ejecutar:
```
λ make compile
```
2. Sólo ejecutar:
```
λ make run
```


## 3. Actualizaciones
Agregar:
* Eliminar clases
* Gestión de paquetes
