#Objetos
OBJS= \
object/Template.o\
object/main.o

BINARY= build/mars
CFLAGS= -Wall -static -static-libgcc -static-libstdc++ -g
CC= g++

all:main
	sudo cp ./build/mars "/usr/bin/"

main: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(BINARY)
	@echo Compilacion exitosa...
	@echo ""

compile: main
	@echo "****************************************************************"
	./build/mars

run:
	@echo "****************************************************************"
	./build/mars

clean:
	rm object/*.o
	rm build/*

object/main.o: main.cpp
	$(CC) $(CFLAGS) -c main.cpp -o object/main.o

object/Template.o: src/Template.h src/Template.cpp
	$(CC) $(CFLAGS) -c src/Template.cpp -o object/Template.o
